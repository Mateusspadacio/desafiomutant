const client = require('../utils/elasticSearchClient');

module.exports = (iteration) => {
    return new Promise((resolve, reject) => {
        client.index({
            index: 'logs',
            body: {
                iteration,
                date: new Date()
            }
        }, (err, response) => {
            if (err) {
                return reject(err);
            }
            return resolve(response);
        });
    });
}