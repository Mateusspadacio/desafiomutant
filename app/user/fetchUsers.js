const axios = require('axios');
const compareStringFn = require('../utils/compareStringFn');

module.exports = async () => {
    const users = await axios.get('https://jsonplaceholder.typicode.com/users').then(response => response.data);
    const filteredUsers = users.filter(user => /suite/ig.test(user.address.suite))
        .map(({ name, email, website, address, company }) => ({ name, email, website, address, company }));
    
    filteredUsers.sort((user1, user2) => compareStringFn(user1.name, user2.name));
    return filteredUsers;
};
