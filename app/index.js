require('dotenv').config()
const express = require('express');
const server = express();
const fetchUsers = require('./user/fetchUsers');
const insertLog = require('./utils/insertLog');

server.get('/', async (req, res) => {
    try {
        const users = await fetchUsers();
        await insertLog('fetchUsers');
        res.status(200).send(users);
    } catch (e) {
        res.status(500);
    }
});

server.listen(process.env.PORT);
