const { expect } = require('chai');
const fetchUsers = require('../../app/user/fetchUsers');
const fetchUsersMock = require('../files/fetchUsersMock.json');


describe('fetchUser', () => {
    it('should fetch users', async () => {
        const users = await fetchUsers();
        expect(users).to.be.an('array');
        expect(users).to.be.deep.equal(fetchUsersMock);
    });
});
